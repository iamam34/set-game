import { useState } from 'react';

export function ChooseUsername(props) {
    const [username, setUsername] = useState("");

    function handleInputChange(event) {
        setUsername(event.target.value)
    }

    function handleSubmit(event) {
        props.websocket.sendMessage(JSON.stringify({
            type: 'set-username',
            data: {
                username: username
            }
        }));
        event.preventDefault();
    }

    return <form onSubmit={handleSubmit} >
        <label>
            What's your name?
            <input type="text" name="username" value={username} onChange={handleInputChange} />
        </label>
        <button type="submit" >Go!</button>
    </form>
}