const { isCorrectSet, generateDeckOfCards } = require('./game-logic');

describe('isCorrectSet', () => {
    describe('correct set', () => {
        test('three very different cards', () => {
            expect(isCorrectSet([
                "0021",
                "1210",
                "2102",
            ])).toBeTruthy();
        });
        test('same color only', () => {
            expect(isCorrectSet([
                "0021",
                "0210",
                "0102",
            ])).toBeTruthy();
        });
        test('same number only', () => {
            expect(isCorrectSet([
                "0021",
                "2010",
                "1002",
            ])).toBeTruthy();
        });
        test('same everything except color', () => {
            expect(isCorrectSet([
                "0000",
                "1000",
                "2000",
            ])).toBeTruthy();
        });
    });

    describe('incorrect set', () => {
        test('three identical cards', () => {
            expect(isCorrectSet([
                "0000",
                "0000",
                "0000",
            ])).toBeFalsy();
        });
        test('two identical cards and a random', () => {
            expect(isCorrectSet([
                "0000",
                "0000",
                "2222",
            ])).toBeFalsy();
        });
        test('semi-unique shapes', () => {
            expect(isCorrectSet([
                "1000",
                "0100",
                "0020",
            ])).toBeFalsy();
        });
        test('four cards', () => {
            expect(isCorrectSet([
                "2100",
                "1100",
                "0100",
                "0020",
            ])).toBeFalsy();
        });
        test('two cards', () => {
            expect(isCorrectSet([
                "0100",
                "0020",
            ])).toBeFalsy();
        });
        test('a card is null', () => {
            expect(isCorrectSet([
                null,
                "0100",
                "0020",
            ])).toBeFalsy();
        });
        test('set is []', () => {
            expect(isCorrectSet([])).toBeFalsy();
        });
    });

    test('error when set is null', () => {
        expect(() => isCorrectSet(null)).toThrowError();
    });
});

describe('deckOfCards', () => {
    var deckOfCards;
    beforeAll(() => {
        deckOfCards = generateDeckOfCards();
    });
    test('has 81 cards', () => {
        expect(deckOfCards).toHaveLength(81);
    });
    test('does not include duplicates', () => {
        expect(new Set(deckOfCards).size).toEqual(81);
    })
    test('includes 0000', () => {
        expect(deckOfCards).toContain("0000");
    });
    test('includes 2101', () => {
        expect(deckOfCards).toContain("2101");
    });
    test('includes 2222', () => {
        expect(deckOfCards).toContain("2222");
    });
    test('does not include 0003', () => {
        expect(deckOfCards).not.toContain("0003");
    });
    test('is shuffled', () => {
        expect(deckOfCards.slice(0, 3)).not.toEqual(["0000", "0001", "0002"]);
    });
});
