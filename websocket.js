const { findASet } = require('./game-logic');
const GameState = require('./game-state');

function configureWsServer(wsServer) {
  const games = {
    "iceberg2": new GameState()
  };
  const usernames = {
    // "uuid-example": "username-example"
  };

  // Set up a headless websocket server to handle incoming messages
  wsServer.on('connection', (websocket, request) => {
    websocket.isAlive = true;
    websocket.on('pong', data => {
      websocket.isAlive = true;
    });

    websocket.userId = request.session.id;
    usernames[websocket.userId] = websocket.userId;

    // register incoming message handler
    websocket.on('message', data => {
      const game = games[websocket.gameId];

      console.log("Received from", websocket.userId, websocket.gameId, data);
      message = JSON.parse(data);

      switch (message.type) {
        case "card-select":
          if (!game) {
            websocket.send(JSON.stringify({
              type: 'error',
              data: {
                message:
                  `You must join a game before selecting a card.`
              }
            }));
            break; // exit switch statement
          }
          if (game.userClaimingSet === null) {
            // this is the first card of a new candidate set
            game.userClaimingSet = websocket.userId;
          } else if (game.userClaimingSet !== websocket.userId) {
            // user is not allowed to claim a set at this time
            // (another user is probably claiming)
            // not a broadcast
            websocket.send(JSON.stringify({
              type: 'rejection',
              data: {
                message:
                  `Sorry, user ${game.userClaimingSet} got in first. Faster next time!`
              }
            }));
            break; // exit switch statement
          }

          game.candidateSet.push(message.data.card);

          if (game.candidateSet.length === 1) {
            // start countdown
            if (game.timeoutForClaimingSet) {
              clearTimeout(game.timeoutForClaimingSet);
            }
            game.timeoutForClaimingSet = setTimeout(
              attemptToClaimSet,
              5000 /* milliseconds until execution */,
              wsServer, websocket, games /* ...args to pass on */
            );
          }

          if (game.candidateSet.length < 3) { // 1 or 2
            // immediately broadcast an update
            wsServer.clients.forEach(c => {
              if (c.gameId === websocket.gameId) {
                const isYou = c === websocket;
                c.send(JSON.stringify({
                  type: 'card-selected',
                  data: {
                    candidateSet: game.candidateSet,
                    userClaimingSet: game.userClaimingSet,
                    isYou: isYou,
                    message: isYou ? `Ok, you are claiming a set. Carry on.` : "Someone else is claiming a set."
                  }
                }));
              }
            });
          } else {
            // we have 3 cards
            if (game.timeoutForClaimingSet) {
              clearTimeout(game.timeoutForClaimingSet);
            }
            attemptToClaimSet(wsServer, websocket, games);
          }
          break;

        case 'reset-game':
          if (!game) {
            websocket.send(JSON.stringify({
              type: 'error',
              data: {
                message:
                  `You must join a game before resetting it.`
              }
            }));
            break; // exit switch statement
          }
          // keep existing users
          game.resetGame();
          // broadcast to all clients
          wsServer.clients.forEach(c => {
            if (c.gameId === websocket.gameId) {
              c.send(JSON.stringify({
                type: 'new-game',
                data: {
                  // send everything
                  cheat: findASet(game.cardsInPlay),
                  cardsInPlay: game.cardsInPlay,
                  scoreboard: game.scoreboard,
                  candidateSet: game.candidateSet,
                  userClaimingSet: game.userClaimingSet,
                  message: 'Right, here\'s a new game.'
                }
              }));
            }
          });
          break;
        case 'join-game':
          const gameId = message.data.gameId;
          if (!gameId) {
            websocket.send(JSON.stringify({
              type: 'error',
              data: {
                message:
                  `Um.. which game do you want to join? Missing 'gameId'.`
              }
            }));
            break; // exit switch statement
          }

          // create game if it doesn't exist
          if (!games.hasOwnProperty(gameId)) {
            games[gameId] = new GameState();
            wsServer.clients.forEach(c => {
              if (c != websocket && !c.gameId) { // to anyone who is NOT in a game
                c.send(JSON.stringify({
                  type: 'game-created',
                  data: {
                    gameIds: Object.keys(games),
                    message: `Somebody created a new game room: '${gameId}'.`
                  }
                }));
              }
            });
          }

          websocket.gameId = gameId;
          const joinedGame = games[gameId];

          // check that user hasn't already joined
          if (joinedGame.users.includes(websocket.userId)) {
            websocket.send(JSON.stringify({
              type: 'error',
              data: {
                message:
                  `You cannot join '${gameId}', because you've already joined it!`
              }
            }));
            break; // exit switch statement
          }

          joinedGame.addUser(websocket.userId);

          const usernamesInGame = getUsernamesInGame(usernames, joinedGame);

          websocket.send(JSON.stringify({
            type: 'game-joined',
            data: {
              // send everything
              gameIds: Object.keys(games),
              gameId: gameId,
              cardsInPlay: joinedGame.cardsInPlay,
              scoreboard: joinedGame.scoreboard,
              candidateSet: joinedGame.candidateSet,
              userClaimingSet: joinedGame.userClaimingSet,
              usernames: usernamesInGame,
              gameIsOver: joinedGame.gameIsOver(),
              cheat: findASet(joinedGame.cardsInPlay),
              message: `Welcome to game ${gameId}.`
            }
          }));

          // broadcast new-player to all other clients
          wsServer.clients.forEach(c => {
            if (c !== websocket && c.gameId === websocket.gameId) {
              c.send(JSON.stringify({
                type: 'new-player',
                data: {
                  scoreboard: joinedGame.scoreboard,
                  usernames: usernamesInGame,
                  message: "Another person joined your game."
                }
              }));
            }
          });
          break;
        case 'leave-game':
          kickPlayerFromGame(wsServer, websocket, games);
          break;
        case 'set-username':
          const newUsername = message.data.username;
          if (!newUsername) {
            websocket.send(JSON.stringify({
              type: 'error',
              data: {
                message:
                  `Um.. what do you want to be called? Missing 'username'.`
              }
            }));
            break; // exit switch statement
          }
          const oldUsername = usernames[websocket.userId];
          usernames[websocket.userId] = newUsername;

          websocket.send(JSON.stringify({
            type: "user-renamed",
            data: {
              userId: websocket.userId,
              username: newUsername,
              message: `I hereby declare you to be '${newUsername}'.`
            }
          }));

          if (game) {
            // tell everyone else in your game
            wsServer.clients.forEach(c => {
              if (c !== websocket && c.gameId === websocket.gameId) {
                c.send(JSON.stringify({
                  type: "user-renamed",
                  data: {
                    usernames: getUsernamesInGame(usernames, game),
                    message: `Player '${oldUsername}' is now called '${newUsername}'.`
                  }
                }));
              }
            });
          }
          break;
        default:
          // unrecognised message type
          websocket.send(JSON.stringify({
            type: 'error',
            data: {
              message:
                `I have no idea what you're trying to do. What is "${message.type}"?!?`
            }
          }));
          break;
      }

    });

    websocket.on('close', (code, reason) => {
      console.log('A client closed their connection', code, reason, websocket.userId);
      kickPlayerFromGame(wsServer, websocket, games);
      websocket.terminate();
    });

    websocket.send(JSON.stringify({
      type: 'welcome',
      data: {
        userId: websocket.userId,
        gameId: websocket.gameId, // send null to override old clientside value
        gameIds: Object.keys(games),
        message: `Welcome, I am a WebSocket server, and you are ${websocket.userId}.`
      }
    }));

  });

  // send ping every 30 seconds
  const pingInterval = setInterval(() => {
    console.log("Sending a ping to", wsServer.clients.size, "client(s)");
    wsServer.clients.forEach(c => {
      if (c.isAlive === false) { // we haven't received a pong recently
        kickPlayerFromGame(wsServer, c, games);

        return c.terminate();
      }

      c.isAlive = false; // expect a pong soon
      c.ping(); // request a pong
    });
  }, 30000); // 30 seconds

  wsServer.on('close', () => {
    // stop pinging
    clearInterval(pingInterval);
  });
}

function getUsernamesInGame(usernames, game) {
  if (!game) {
    return {};
  }
  let usersInGame = {};
  for (const userId of Object.keys(usernames)) {
    if (game.isUserInGame(userId)) {
      usersInGame[userId] = usernames[userId];
    }
  }
  return usersInGame;
}

function kickPlayerFromGame(wsServer, websocket, games) {
  if (websocket.gameId) {
    const game = games[websocket.gameId];
    game.removeUser(websocket.userId);
    websocket.gameId = null;

    // tell everyone else they're gone
    const gameUsers = game.users; // calculate only once
    wsServer.clients.forEach(client => {
      if (client !== websocket && gameUsers.includes(client.userId)) {
        client.send(JSON.stringify({
          type: 'remove-user',
          data: {
            scoreboard: game.scoreboard,
            message: "Someone left your game."
          }
        }));
      }
    });
  }

  websocket.send(JSON.stringify({
    type: 'game-left',
    data: {
      gameIds: Object.keys(games),
      // reset everything
      gameId: null,
      cardsInPlay: [],
      scoreboard: {},
      candidateSet: [],
      userClaimingSet: null,
      cheat: [],
      message: `You have left your game.`
    }
  }));


  // TODO consider deleting game if there are no users left.
}

function attemptToClaimSet(wsServer, websocket, games) {
  if (!websocket.gameId) {
    throw Error("Join a game before claiming a set");
  }

  const game = games[websocket.gameId];
  const gameUsers = game.users; // calculate only once

  const attemptedSet = [...game.candidateSet]; // clone
  const isCorrect = game.claimSet().isCorrect;
  const gameIsOver = game.gameIsOver();

  // broadcast to all clients
  wsServer.clients.forEach(client => {
    if (gameUsers.includes(client.userId)) {
      const isYou = client === websocket;
      client.send(JSON.stringify({
        type: 'set-claim-attempt',
        data: {
          cheat: findASet(game.cardsInPlay),
          cardsInPlay: game.cardsInPlay,
          scoreboard: game.scoreboard, // includes latest claimed set if isCorrect
          candidateSet: game.candidateSet, // now empty
          attemptedSet: attemptedSet,
          userClaimingSet: game.userClaimingSet, // now null
          isCorrect: isCorrect,
          isYou: isYou,
          gameIsOver: gameIsOver,
          message: isYou
            ? (isCorrect ? "Congratulations! You have a set." : "Muurrrp! Not a set.")
            : (isCorrect ? "Someone else claimed a set." : "Someone else made a mistake.")
        }
      })
      )
    };

    if (gameIsOver) {
      client.send(JSON.stringify({
        type: 'game-over',
        data: {
          scoreboard: game.scoreboard,
          message: "Game over (but this message is deprecated)."
        }
      }));
    }
  });
}

module.exports = { configureWsServer };