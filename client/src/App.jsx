import './App.css';

import { Game } from './components/Game';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>SET</h1>
      </header>
      <Game />
      <footer className="App-footer">
        <ul>
          <li>Card design © Copyright 1974 Marsha Falco</li>
          <li>Card images by <a href="//commons.wikimedia.org/wiki/User:Cmglee" title="User:Cmglee">Cmglee</a> - <span class="int-own-work" lang="en">Own work</span>, <a href="https://creativecommons.org/licenses/by-sa/4.0" title="Creative Commons Attribution-Share Alike 4.0">CC BY-SA 4.0</a> on <a href="https://commons.wikimedia.org/w/index.php?curid=66776401">Wikimedia</a></li>
          <li>SVG Help from <a href="https://svgpocketguide.com/book/">SVG Pocket Guide</a></li>
        </ul>
      </footer>
    </div>
  );
}

export default App;
