var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// the output of client `yarn build-client` goes into ./client/build
app.use(express.static(path.join(process.cwd() + "/client/build")));

module.exports = app;
