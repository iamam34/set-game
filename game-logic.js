function isCorrectSet(candidateSet) {
    if (candidateSet && candidateSet.filter(card => card).length !== 3) {
        return false; // we require 3 truthy cards
    }

    // (in no particular order) "number", "color", "shading", "shape"
    dimensions = [0, 1, 2, 3];
    var threeIdenticalCards = true;

    for (var dimension of dimensions) {
        const set = new Set(candidateSet.map(card => card[dimension]));
        // set's size will be 1 if all the same, or 3 if all different

        if (set.size === 2) {
            return false;
        } else if (set.size === 3) {
            threeIdenticalCards = false; // phew we found a difference
        }
    }
    return !threeIdenticalCards;
}

function generateDeckOfCards() {
    function permute(permutations, atoms, length) {
        if (length === 0) {
            return permutations;
        } else {
            newPermutations = [];
            for (permutation of permutations) {
                newPermutations.push(...(atoms.map(atom => permutation + atom)))
            }
            return permute(newPermutations, atoms, length - 1);
        }
    }
    const deckOfCards = permute([""], ["0", "1", "2"], 4);

    shuffle(deckOfCards);

    return deckOfCards;
}

function shuffle(cards) {
    // Fisher-Yates shuffle (in-place)
    // from https://github.com/einaregilsson/cards.js/blob/1dd6db6ea2f7b84b86b2930eec299369e186f957/cards.js#L82
    var i = cards.length;
    if (i == 0) return;
    while (--i) {
        var j = Math.floor(Math.random() * (i + 1));
        var tempi = cards[i];
        var tempj = cards[j];
        cards[i] = tempj;
        cards[j] = tempi;
    }
}

function findASet(cards) {
    var set = null;
    outerLoop: for (var i = 0; i < cards.length; i++) {
        for (var j = i + 1; j < cards.length; j++) {
            for (var k = j + 1; k < cards.length; k++) {
                candidateSet = [cards[i], cards[j], cards[k]];
                if (isCorrectSet(candidateSet)) {
                    set = candidateSet; // there is a set on the table!
                    break outerLoop;
                }
            }
        }
    }
    return set;
}


module.exports = { isCorrectSet, generateDeckOfCards, findASet };