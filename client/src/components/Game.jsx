import { useState, useRef } from 'react';
import Websocket from 'react-websocket';

import { Card } from './Card';
import { Menu } from './Menu';
import { ChooseUsername } from './ChooseUsername';

import './Game.css';

export function Game() {
    const [messages, setWebsocketMessages] = useState([]);
    const wsRef = useRef(null);
    const [cardsInPlay, setCardsInPlay] = useState([]);
    const [candidateSet, setCandidateSet] = useState([]);
    const [scores, setScores] = useState({});
    const [userId, setUserId] = useState(null);
    const [deniedSet, setDeniedSet] = useState([]);
    const [acceptedSet, setAcceptedSet] = useState([]);
    const [lockScreen, setLockScreen] = useState(false);
    const [answerSet, setAnswerSet] = useState([]);
    const [countDownDisplay, setCountDownDisplay] = useState(false);
    const [gameId, setGameId] = useState(null);
    const [gameIds, setGameIds] = useState([]);

    const handleData = (data) => {
        const result = JSON.parse(data);

        if (result.data.candidateSet) {
            setCandidateSet(result.data.candidateSet);
            // Display or hide count down timer
            if (result.data.candidateSet.length === 1 && result.data.isYou) {
                setCountDownDisplay(true);
            } else if (result.data.candidateSet.length === 0) {
                setCountDownDisplay(false);
            }
        }
        if (result.data.userId) {
            setUserId(result.data.userId);
        }
        if (result.data.scoreboard) {
            var incomingScores = {};
            for (const [userId, userScore] of Object.entries(result.data.scoreboard)) {
                incomingScores[userId] = `${userScore.sets.length} - ${userScore.mistakes} = ${userScore.sets.length - userScore.mistakes}`;
            }
            setScores(incomingScores);
        }
        if (result.data.cheat) {
            setAnswerSet(result.data.cheat);
        }
        if (result.data.hasOwnProperty("gameId")) { // may be null
            setGameId(result.data.gameId);
        }
        if (result.data.gameIds) {
            setGameIds(result.data.gameIds);
        }

        if (result.type === 'new-game') {
            setLockScreen(false);
        } else if (result.type === 'set-claim-attempt') {
            setLockScreen(false);
            if (result.data.isCorrect) {
                const accepted = result.data.attemptedSet.map(card => {
                    const cardIndex = cardsInPlay.indexOf(card);
                    return {
                        definition: card,
                        x: Math.floor(cardIndex / 3) + 1,
                        y: cardIndex % 3 + 1
                    };
                });
                setAcceptedSet(accepted);
            } else {
                setDeniedSet(result.data.attemptedSet);
            }
        } else if (result.type === 'card-selected') {
            setLockScreen(!result.data.isYou);
        }
        if (result.data.cardsInPlay) {
            setCardsInPlay(result.data.cardsInPlay);
        }
        const stringMessage = `${result.data.message} {${Object.keys(result.data)}}`;
        setWebsocketMessages([stringMessage, ...messages])
    }

    function handleAnimationEnd(event) {
        const cardDefinition = event.currentTarget.dataset.cardDefinition;

        // Check for denied animation
        const cardDeniedIndex = deniedSet.indexOf(cardDefinition)
        if (cardDeniedIndex >= 0) {
            const newDeniedSet = [...deniedSet];
            newDeniedSet.splice(cardDeniedIndex, 1);
            setDeniedSet(newDeniedSet);
        }

        // Check for remove animation
        const cardAcceptedIndex = acceptedSet.findIndex(cardData => cardData.definition === cardDefinition)
        if (cardAcceptedIndex >= 0) {
            const newAcceptedSet = [...acceptedSet];
            newAcceptedSet.splice(cardAcceptedIndex, 1);
            setAcceptedSet(newAcceptedSet);
        }
    }

    function handleClickOnCard(event) {
        if (lockScreen) {
            // Someone else is selecting a set
            return;
        }
        const cardDefinition = event.currentTarget.dataset.cardDefinition;
        if (candidateSet.includes(cardDefinition)) {
            return;
        }
        wsRef.current.sendMessage(JSON.stringify({
            type: 'card-select',
            data: {
                card: cardDefinition
            }
        }));
    };

    function resetGame() {
        wsRef.current.sendMessage(JSON.stringify({
            type: 'reset-game'
        }));
    }

    function leaveGame() {
        wsRef.current.sendMessage(JSON.stringify({
            type: 'leave-game',
        }));
    }

    return <>
        <Websocket url={`ws://${window.location.hostname}:3001`} // TODO use secure 'wss://' protocol
            reconnect={true}
            debug={true}
            onMessage={handleData}
            ref={wsRef} />
        {gameId
            ? <>
                <div className="scores" >
                    <h2>Scores</h2>
                    <ul>
                        {Object.entries(scores).map(([scoreUserId, score], index) => {
                            const userTag = scoreUserId.substring(0, 3);
                            const username = userId === scoreUserId ? `Me (${userTag})` : `Player ${userTag}`;
                            return <li key={scoreUserId}>{username}: {score}</li>;
                        })}
                    </ul>
                </div>
                <button onClick={resetGame}>New Game</button>
                <button onClick={leaveGame}>Leave Game</button>
                <div className={"grid" + (lockScreen ? " grid-lock" : "") + (countDownDisplay ? " grid-timer" : "")}>
                    {cardsInPlay.map((card, index) => {
                        const inAcceptedSet = (0 <= acceptedSet.findIndex(val => {
                            return val.x === Math.floor(index / 3) + 1 && val.y === index % 3 + 1;
                        }));
                        return <Card key={card} handleClick={handleClickOnCard} wiggle={deniedSet.includes(card)} handleAnimationEnd={handleAnimationEnd} definition={card} isSelected={candidateSet.includes(card)} adding={inAcceptedSet} answer={answerSet.includes(card)} />;
                    })}
                    {acceptedSet.map(cardData =>
                        <Card key={cardData.definition} x={cardData.x} y={cardData.y} removing={true} definition={cardData.definition} handleAnimationEnd={handleAnimationEnd} />
                    )}
                </div>
            </>
            : <Menu gameIds={gameIds} websocket={wsRef.current} />}
            <ChooseUsername websocket={wsRef.current} />
        <div className="messages">
            {messages.map((message, index) => (
                <p key={index}>{message}</p>
            ))}
        </div>
    </>;
}
