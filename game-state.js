const { isCorrectSet, generateDeckOfCards, findASet } = require("./game-logic");

class GameState {

    get users() {
        return Object.keys(this.scoreboard);
    }

    constructor() {
        this.timeoutForClaimingSet = null;
        this.scoreboard = {}; /* FORMAT: { "userId": { sets: [[card,card,card]], mistakes: 0 } } */
        this.resetGame();
    }

    resetGame() {
        this._deck = generateDeckOfCards();
        this.userClaimingSet = null;
        this.candidateSet = []; /* FORMAT: [card, card, card] */

        // reset user's sets and mistakes
        for (var userId of this.users) {
            this.removeUser(userId);
            this.addUser(userId);
        };

        // deal the top 12 cards onto the table
        this.cardsInPlay = this._deck.splice(-12, 12);
        this.ensurePlayable();

        return {
            deck: this._deck,
            userClaimingSet: this.userClaimingSet,
            candidateSet: this.candidateSet,
            cardsInPlay: this.cardsInPlay,
            scoreboard: this.scoreboard,
        };
    }

    claimSet() {
        // add the claimed cards to user's scoreboard
        const userId = this.userClaimingSet;
        if (!userId || !this.users.includes(userId)) {
            throw Error(`this.userClaimingSet '${userId}' must join the game before claiming a set`);
        }

        const isCorrect = isCorrectSet(this.candidateSet);
        if (isCorrect) {
            if (this.cardsInPlay.length > 12) {
                // remove the claimed cards
                for (const card of this.candidateSet) {
                    this.cardsInPlay.splice(this.cardsInPlay.indexOf(card), 1);
                }
            } else {
                // replace the claimed cards with new cards from the deck
                for (const card of this.candidateSet) {
                    const newCard = this._deck.pop();
                    if (newCard) {
                        // replace card
                        this.cardsInPlay[this.cardsInPlay.indexOf(card)] = newCard;
                    } else {
                        // no more cards in deck, just remove the card
                        this.cardsInPlay.splice(this.cardsInPlay.indexOf(card), 1);
                    }
                }
            }
            this.ensurePlayable();

            this.scoreboard[userId].sets.push(this.candidateSet);
        } else {
            this.scoreboard[userId].mistakes++;
        }

        // reset, ready for next time
        this.candidateSet = [];
        this.userClaimingSet = null;

        if (isCorrect) {
            return {
                isCorrect: isCorrect,
                candidateSet: this.candidateSet,
                userClaimingSet: this.userClaimingSet,
                scoreboard: this.scoreboard,
                deck: this._deck,
                cardsInPlay: this.cardsInPlay,
            };
        } else {
            return {
                isCorrect: isCorrect,
                candidateSet: this.candidateSet,
                userClaimingSet: this.userClaimingSet,
                scoreboard: this.scoreboard,
                // deck has not changed
                // cardsInPlay has not changed
            };
        }
    }

    ensurePlayable() {
        // If there are no sets on the table, add 3 more cards.
        while (!findASet(this.cardsInPlay) && this._deck.length > 0) {
            // move three cards from deck to cardsInPlay
            // if deck is empty this does nothing (splice returns empty array)
            this.cardsInPlay.push(...this._deck.splice(-3, 3));
        }

        return {
            deck: this._deck,
            cardsInPlay: this.cardsInPlay,
        };
    }

    gameIsOver() {
        // return bool
        return this._deck.length === 0 && !findASet(this.cardsInPlay);
    }

    isUserInGame(userId) {
        return this.scoreboard.hasOwnProperty(userId);
    }

    addUser(userId) {
        if (!this.isUserInGame(userId)) {
            this.scoreboard[userId] = {
                sets: [],
                mistakes: 0
            }
        }

        return {
            scoreboard: this.scoreboard,
        };
    }

    removeUser(userId) {
        if (this.isUserInGame(userId)) {
            delete this.scoreboard[userId];
        }

        return {
            scoreboard: this.scoreboard,
        };
    }
}

module.exports = GameState;