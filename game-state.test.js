const { findASet } = require('./game-logic');
const GameState = require('./game-state');

describe('GameState', () => {
    test('deals 12 cards to 0 users to begin', () => {
        const game = new GameState();
        overrideToMakePlayable(game);

        expect(game.cardsInPlay).toHaveLength(12);
        expect(game._deck).toHaveLength(81 - 12);
        expect(game.users).toHaveLength(0);
        expect(game.scoreboard).toEqual({});
    });
    test('new game resets all variables except users', () => {
        const game = new GameState();
        overrideToMakePlayable(game);
        game.addUser("user-amy");

        game.userClaimingSet = "user-amy";
        game.candidateSet = findASet(game.cardsInPlay);
        game.claimSet();

        game.resetGame();

        expect(game.cardsInPlay).toHaveLength(12);
        expect(game._deck).toHaveLength(81 - 12);
        expect(game.scoreboard).toEqual({ 
            "user-amy": {
                sets: [],
                mistakes: 0
            }
        });
        expect(game.candidateSet).toEqual([]);
        expect(game.users).toEqual(["user-amy"]);
        expect(game.userClaimingSet).toEqual(null);
    });
    test('new game is always playable', () => {
        // WARNING: this test can produce false negatives. This test will fail every now and again (about 1 in 15) if code is broken.
        // If test fails once, there is definitely a problem (even if it passes the next time).
        const game = new GameState();

        const expectedSet = findASet(game.cardsInPlay);
        expect(expectedSet).toBeTruthy();
    })
    test('ensures game is playable', () => {
        const game = new GameState();
        overrideToMakeUnplayable(game);

        // double-check there really is no set
        const unexpectedSet = findASet(game.cardsInPlay);
        expect(unexpectedSet).toBeFalsy();

        game.ensurePlayable();

        expect(game.cardsInPlay).toHaveLength(15);
        expect(game._deck).toHaveLength(81 - game.cardsInPlay.length);
        const expectedSet = findASet(game.cardsInPlay);
        expect(expectedSet).toBeTruthy();
    });

    test('ensurePlayable() does nothing when deck is exhausted', () => {
        const game = new GameState();
        overrideToMakeUnplayable(game);
        game._deck = [];

        game.ensurePlayable();

        expect(game._deck).toHaveLength(0);
        expect(game.cardsInPlay).toHaveLength(12); // not more
    });
    describe('claimSet()', () => {
        test('stores candidateSet', () => {
            const game = new GameState();
            overrideToMakePlayable(game);
            const theSet = findASet(game.cardsInPlay);

            game.addUser("user-amy");
            game.userClaimingSet = "user-amy";
            game.candidateSet = theSet;
            game.claimSet();

            expect(game.cardsInPlay).toHaveLength(12);
            expect(game._deck).toHaveLength(81 - 12 - 3);
            expect(game.scoreboard).toEqual({
                "user-amy": {
                    sets: [theSet],
                    mistakes: 0
                }
            });
        });
        test('does not replace extra column of cards', () => {
            const game = new GameState();
            game.addUser("user-amy");
            overrideToMakeUnplayable(game);
            game.ensurePlayable(); // add extra cards

            expect(game.cardsInPlay).toHaveLength(15); // enlarged

            game.userClaimingSet = "user-amy";
            game.candidateSet = findASet(game.cardsInPlay);
            game.claimSet();

            expect(game.cardsInPlay).toHaveLength(12); // back to normal
        });
        test('does not replace cards when deck is exhausted', () => {
            const game = new GameState();
            game.addUser("user-amy");

            game._deck = [];
            const cardsInPlay = [...game.cardsInPlay];

            game.userClaimingSet = "user-amy";
            game.candidateSet = findASet(game.cardsInPlay);
            game.claimSet();

            expect(game._deck).toHaveLength(0);
            expect(game.cardsInPlay).toHaveLength(cardsInPlay.length - 3);
        });

        test('resets values and counts sets when claim is correct', () => {
            const game = new GameState();
            game.addUser("user-amy");

            game.userClaimingSet = "user-amy";
            const set = findASet(game.cardsInPlay);
            game.candidateSet = set;
            game.claimSet();

            expect(game.candidateSet).toEqual([]);
            expect(game.userClaimingSet).toEqual(null);
            expect(game.scoreboard).toEqual({
                "user-amy": {
                    sets: [set],
                    mistakes: 0
                }
            });
        });

        test('resets values and counts mistake when claim is incorrect', () => {
            const game = new GameState();
            game.addUser("user-amy");

            game.userClaimingSet = "user-amy";
            game.candidateSet = findASet(game.cardsInPlay).splice(0, 2); // only 2 cards
            game.claimSet();

            expect(game.candidateSet).toEqual([]);
            expect(game.userClaimingSet).toEqual(null);
            expect(game.scoreboard).toEqual({
                "user-amy": {
                    sets: [],
                    mistakes: 1
                }
            });
        });
    });

    describe('game is NOT over', () => {
        test('when deck is exhausted but there is a set in play', () => {
            const game = new GameState();
            game._deck = [];

            expect(game.gameIsOver()).toBeFalsy();
        });
        test('when there are no sets but there are cards in the deck', () => {
            // I'm pretty sure this is an impossible situation, but 
            // the deck length is still a nice shortcut for quick evaluation (heuristic)
            const game = new GameState();
            overrideToMakeUnplayable(game);

            expect(game.gameIsOver()).toBeFalsy();
        });
    });
    describe('game is over', () => {
        test('when deck is exhausted and there are no sets in play', () => {
            const game = new GameState();
            overrideToMakeUnplayable(game);
            game._deck = [];

            expect(game.gameIsOver()).toBeTruthy();
        });
    });
});

function overrideToMakeUnplayable(game) {
    // ASSUMPTION: no sets have been claimed i.e. all cards are in deck or cardsInPlay.

    const CARDS_IN_PLAY_UNPLAYABLE = [
        "0000", "0200", "2100", "1101",
        "2001", "2201", "0002", "0202",
        "2102", "0111", "2111", "2001",
    ];
    const TOP_CARDS_OF_DECK = ["0100", "1111"]; // to resolve two more sets, to prevent another extra row

    // reconfigure game to be unplayable
    // move all cardsInPlay back into deck
    game._deck.push(...game.cardsInPlay);
    game.cardsInPlay = [];
    // move unplayable cards from deck to cardsInPlay
    for (var card of CARDS_IN_PLAY_UNPLAYABLE) {
        game._deck.splice(game._deck.indexOf(card), 1);
        game.cardsInPlay.push(card);
    }

    // set up top card in deck to resolve a set
    for (var card of TOP_CARDS_OF_DECK) {
        game._deck.push(...game._deck.splice(game._deck.indexOf(card), 1));
    }

    // double-check
    expect(findASet(game.cardsInPlay)).toBeFalsy();
}

function overrideToMakePlayable(game) {
    // ASSUMPTION: no sets have been claimed i.e. all cards are in deck or cardsInPlay.

    const CARDS_IN_PLAY_PLAYABLE = [
        "0000", "0100", "0200", "1101",
        "2001", "2201", "0002", "0202",
        "2102", "0111", "2111", "1111",
    ];

    // reconfigure game to be playable
    // move all cardsInPlay back into deck
    game._deck.push(...game.cardsInPlay);
    game.cardsInPlay = [];
    // move unplayable cards from deck to cardsInPlay
    for (var card of CARDS_IN_PLAY_PLAYABLE) {
        game._deck.splice(game._deck.indexOf(card), 1);
        game.cardsInPlay.push(card);
    }

    // double-check
    expect(findASet(game.cardsInPlay)).toBeTruthy();
}