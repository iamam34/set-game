# Set Game

Play the classic game of SET online with friends!

Written in JavaScript with a React client and an Express server.

Uses a spritesheet for rendering the deck of cards.

## Development

### Install

Server: `yarn install`

Client: `yarn install-client`

### Run

Server: `yarn start-server`

Client: `yarn start-client`

## Acknowledgements
Original game designed by Marsha Falco 1974

Card images derived from an SVG by Cmglee, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=66776401


![Screenshot](./Screenshot.png)
