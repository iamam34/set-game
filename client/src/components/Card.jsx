import './Card.css';

export function Card(props) {
    const cardWidth = 40;
    const cardHeight = 58;
    const aspectRatio = cardHeight / cardWidth;

    const color = Number(props.definition[0]);
    const shading = Number(props.definition[1]);
    const number = Number(props.definition[2]);
    const shape = Number(props.definition[3]);

    // locate this card inside the spritesheet
    const xIndex = (color * 3 + shading);
    const yIndex = (number * 3 + shape);

    return <div className={"card" +
            (props.isSelected ? " selected-card" : "") +
            (props.wiggle ? " card-wiggle" : "") +
            (props.removing ? " card-disappear" : "") +
            (props.adding ? " card-appear" : "") +
            ((props.answer && window.useCheats) ? " card-answer" : "")}
        onClick={props.handleClick}
        onAnimationEnd={props.handleAnimationEnd}
        data-card-definition={props.definition}
        style={{
            paddingBottom: `${aspectRatio * 100}%`,
            gridColumn: props.x,
            gridRow: props.y
        }}>
        <img src="./cards.svg"
            alt={props.definition}
            style={{
                width: `${9 * 100}%`,
                marginLeft: `${-xIndex * 100}%`,
                marginTop: `${-yIndex * 100 * aspectRatio}%`,
            }} />
    </div>
}