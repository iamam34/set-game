# API Documentation

## Server Response Object

```jsonc
{
    "type": "", /* one of the predefined values */
    "message": "hello world", /* human-readable description */
    "data": {
        /* These attributes appear only if you are NOT in a room */
        "roomIds": [ "iceberg2", "pancakes" ],
    
        /* These attributes appear only if you ARE in a room */
        "room": {
            "roomId": "iceberg2",
            "cardsInPlay": ["2101", "2020", /* ... */ ], /* cards in grid on table (usually 12, sometimes 15 or 18) */
            "deckSize": 81, /* max 81, decreases when cards are dealt into play */
            "candidateSet": [ "2220", ], /* selected cards */
            "failedSet": [ "0001", "0002", "2002" ], /* cards that were recently selected but weren't correct */
            "cheat": [ "2220", "2221", "2222" ], /* a true set, ready to be claimed from the cardsInPlay */
            "scoreboard": [
                {
                    "userId": "_njkaw89erl", /* generated, immutable */
                    "username": "crocodillo", /* user chooses this, defaults to truncated userId */
                    "sets": [
                        ["0201", "0101", "0001"],
                        /* [card, card, card] */
                    ],
                    "mistakes": 0 /* positive integer */
                },
                /* { userId, username, sets, mistakes } of all players in room */
            ],
            "isGameOver": true, /* is the game over? */
            "isSetCorrect": true, /* claimed cards are a true set? */
            "isYou": true, /* are you the one claiming a set? */
        }
    }
}
```

## Message Types 

from Client to Server
- `card-select`
- `reset-game` reset scoreboard, deck and cardsInPlay
- `join-game`
- `leave-game`
- `set-username`

from Server to Client
- `error` unexpected request
- `rejection` race condition violation
- `user-renamed`
- `game-created`
- `game-joined`
- `game-left`
- `new-player` a user has joined your game
- `remove-user` a user has left your game
- `new-game` game has been reset
- `welcome` at first connection to server
- `card-selected` first or second card of a set
- `set-claim-attempt` third card of a set - correct or incorrect!
- `game-over` (deprecated)