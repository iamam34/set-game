import { useState } from 'react';

export function Menu(props) {
    const [gameId, setGameId] = useState("");

    function joinGame(event) {
        props.websocket.sendMessage(JSON.stringify({
            type: 'join-game',
            data: {
                gameId: gameId
            }
        }));
        event.preventDefault();
    };

    function handleInputChange(event) {
        setGameId(event.target.value)
    }

    return <form onSubmit={joinGame} >
        <label>
            Create or Join Game:
            <input type="text" name="gameId" value={gameId} onChange={handleInputChange} list="gameIds" />
            <datalist id="gameIds" >
                {props.gameIds.map(gameId =>
                    <option key={gameId} value={gameId} />
                )}
            </datalist>
        </label>
        <button type="submit" >Go!</button>
    </form>
}